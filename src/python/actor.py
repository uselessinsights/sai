'''
SAI based ACTOR class that is (will be) able to make decisions in order to
survive as long as possible using its experience from past.
'''

# {MSG} TODO: save logs in ACTOR's personal folder

# {TRG} TODO: consider state and environment

# {EXP} TODO: use history

# {TRG} & {EXP} NOTE: some NN may help?

# {CALL}[1] TODO: work with environment

# {CALL}[6] TODO: form history and etc.



# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

# {IMPORT}

import os
import json
import pprint
from pipe import *
from random import choice, randrange

# {HELPERS}

def JL(path):
    with open(path) as F:
            return json.load(F)


def JS(data, path):
    with open(path, 'w') as F:
        F.write(json.dumps(data))

@Pipe
def X(x):
    pprint.pprint(x)
    return x

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

class Actor:

    # {INIT} initialize new ACTOR
    def __init__(self,
                 P_Path='init/params.json',
                 A_Path='init/actions.json'):

        # set unique id
        while True:
            self.ID = str(randrange(10000, 99999))
            if not os.path.exists(self.ID):
                break

        print(f'\nINITIALIZING ACTOR @' + self.ID)
        print('-' * 52)

        # init parameters
        state = JL(P_Path)

        self.P = [*state]
        self.C = self.P \
                    | where(lambda x: x[0].isupper()) \
                    | as_list

        print('==> C & P:')
        self.C |X
        self.P |X

        # init actions
        self.A = JL(A_Path)

        print('==> A:')
        self.A |X

        # init history
        self.H = list([state, None, None])

        print('==> H:')
        self.H |X

        # create state file
        os.makedirs(self.ID)
        JS(state, self.ID + '/state.json')

        print('-' * 52)





    # {MSG} ACTOR's message function
    def M(self, text):
        print('@' + self.ID, '::', text)

    # {TRG} target (param chooser) function [WIP]
    def T(self):
        return choice(self.P)


    # {EXP} experience (action chooser) function [WIP]
    def E(self, ctx):
        return choice(self.A)





    # {CALL}
    def __call__(self):

        self.M('[BORN]')

        while True:

            # [1] get conditions
            sta = JL(self.ID + '/state.json') |X
            env = None

            # [2] survive?
            if not self.C \
                    | where(lambda x: sta[x] > 0) \
                    | as_list: #|X
                break

            # [3] choose target param
            target = self.T()

            # [4] choose best action
            react = self.E(target)

            # [5] perform an action
            self.M(react)

            # [6] prepare for next turn
            input()

        self.M('[DEAD]')



# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

a = Actor()
a()














