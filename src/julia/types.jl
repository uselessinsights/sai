import YAML: load_file, write_file

# *                                                                            ACTION

const Action = Array{String}

# *                                                                            STATE

struct State # XXX ?? dataframe
    major::Dict{String,Real}
    minor::Dict{String,Real}
end

function State(data::Dict)
    return State(data["major"], data["minor"])
end

# *                                                                            CASE

struct Case
    prev::State
    next::State
    act::Action
    wgt::Real
    # XXX linkage to other memories
end

# *                                                                            MEMORY

const Memory = Array{Case}

# *                                                                            ACTOR

mutable struct Actor
    id::String
    A::Action
    S::State
    M::Memory

    "create new actor"
    function Actor()
        isdir("data") || mkdir("data")
        
        id = string(rand(UInt8))

        while true
            id = string(rand(UInt8))
            isdir(id) || break
        end
            
        println("[\33[1m$id\33[0m]: building..")
        mkdir("data/$id")

        try
            write_file(
                "data/$id/state.yml",
                Dict(
                    :major => Dict(string.(collect('a':'e')) .=> rand(Float16, 5)),
                    :minor => Dict(string.(collect('f':'j')) .=> rand(Float16, 5))
                )
            )

            write_file(
                "data/$id/action.yml", 
                string.(collect(1:5))
            )

            println("edit actor's data & press ENTER")
            readline()

            action = Action(load_file("data/$id/action.yml"))
            state = State(load_file("data/$id/state.yml"))
            memory = Memory([])
            return new(id, action, state, memory)
        catch e
            rm(id, force=true, recursive=true)
            throw(e)
        end
    end

    "load existing actor"
    function Actor(id::String)
        isdir("data/$id") || throw("wrong id")

        println("[\33[1m$id\33[0m]: loading..")

        action = Action(load_file("data/$id/action.yml"))
        state = State(load_file("data/$id/state.yml"))
        memory = Memory([]) # TODO load memory
        return new(id, action, state, memory)
    end

    "load existing actor"
    function Actor(id::Integer)
        return Actor(string(id))
    end
end


# quick actor remove
macro rma(id::Integer)
    if isdir("data/$id")
        rm("data/$id", force=true, recursive=true)
    else
        throw("wrong id")
    end
end

macro rma()
    println("\33[1m", "Actors:", "\33[0m")
    for i = readdir("./data")
        println("    \33[2m", i, "\33[0m")
    end
end